package com.devcamp.jbr50;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr50Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr50Application.class, args);
	}

}
