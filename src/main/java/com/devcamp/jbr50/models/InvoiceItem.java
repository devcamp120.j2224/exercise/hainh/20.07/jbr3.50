package com.devcamp.jbr50.models;

public class InvoiceItem {
    private int id;
    private String  desc;
    private int qty;
    private double unitPrice;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public int getQty() {
        return qty;
    }
    public void setQty(int qty) {
        this.qty = qty;
    }
    public double getUnitPrice() {
        return unitPrice;
    }
    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }
    public InvoiceItem(int id, String desc, int qty, double unitPrice) {
        this.id = id;
        this.desc = desc;
        this.qty = qty;
        this.unitPrice = unitPrice;
    }

    @Override
    public String toString(){
        return String.format("InvoiceItem[ id = %s, desc = %s, qty = %s, unitPrice = %s]" , id , desc , qty , unitPrice);
    }
}
